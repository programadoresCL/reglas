Normas Generales de la comunidad
================================

## 1. Propósito

Uno de los objetivos principales de Programadores Chile es fomentar una comunidad abierta, con los más variados y diversos trasfondos posibles. En este sentido, como contribuidores y administradores estamos comprometidos de promover un ambiente amistoso, seguro y acogedor para tod@s, independiente de su género, orientación sexual, capacidades, etnia, estatus socioeconómico y religión (o la ausencia de esta).

Este código de conducta esboza nuestras espectativas para tod@s aquell@s que participan en nuestra comunidad, como también las consecuencias que poseen los comportamientos que se consideran inaceptables.

Invitamos a tod@s aquell@s que participan en la comunidad de Programadores Chile a que nos ayuden a crear experiencias seguras y positivas para tod@s.

## 2. Open Source y Cultura

Un objetivo complementario de este código de conducta es desarrollar la cultura Open Source y el espíritu tecnológico, animando a l@s participantes a reconocer y fortalecer las relaciones entre sus acciones y las consecuencias que estas generan en nuestra comunidad.

Las comunidades reflejan las sociedades en las que habitan y las acciones positivas son esenciales para poder contrarrestar las múltiples formas de desigualdad y abusos de poder que puedan existir en la sociedad.

Si ves a alguien que esta haciendo un esfuerzo extra para asegurar que nuestra comunidad sea acogedora, amistosa, y además, promueve a todos l@s participantes a contribuir al máximo, queremos saberlo.

## 3. Comportamiento esperado

Se esperan y solicitan los siguientes comportamientos para todos los miembros de la comunidad:

* Uso de lenguaje amable e inclusivo.
* Mostrar empatía a otros miembros de la comunidad.
* Participar de una manera auténtica y activa. Al hacerlo, se contribuye al bienestar y la longevidad de esta comunidad.
* Ejercer la consideración y el respeto en los discursos y acciones.
* Aceptación de críticas constructivas.
* Respeto a diferentes puntos de vista y experiencias.
* Practicar la colaboración por sobre el conflicto.
* Enfocarse en lo que es mejor para la comunidad
* Abstenerse de comportamientos y discursos humillantes, discriminatorios, o acosadores.
* Ser consciente de su entorno y de sus compañer@s participantes. Alerta a los líderes de la comunidad si notas una situación conflictiva, alguien que actúe de manera ofensiva, o cualquier violación de este Código de Conducta, incluso si estos parecen ser irrelevantes.
* Recuerde que los lugares de los eventos de la comunidad pueden ser compartidos con miembros de manera pública; Por favor sea respetuos@ con tod@s l@s asistentes a estos lugares.
* Si una publicación no sigue el presente Código de Conducta, tiene el deber de reportarlo para su posterior revisión.

## 3.1 Diez mandamientos

Se espera que cada miembro de la comunidad cumpla los 10 Mandamientos de la Ética Informática:

* I.    No usarás una computadora para dañar a otros.
* II.   No interferirás con el trabajo ajeno.
* III.  No indagarás en los archivos ajenos.
* IV.   No utilizarás una computadora para robar.
* V.    No utilizarás la informática para realizar fraudes.
* VI.   No violarás las licencias del o los software que utilizas.
* VII.  No utilizarás los recursos informáticos ajenos sin la debida autorización.
* VIII. No te apropiarás de los derechos intelectuales de otros.
* IX.   Deberás evaluar las consecuencias sociales de cualquier código que desarrolles.
* X.    Siempre utilizarás las computadoras de manera de respetar los derechos de los demás.


## 4. Comportamiento Inaceptable

Los siguientes comportamientos son considerados acoso y son inaceptables dentro de nuestra comunidad:

* Violencia, amenazas de violencia, mofa o lenguaje violento dirigido contra otra persona.
* Chistes o lenguaje sexista, racista, homofóbico, transfóbico, en contra de personas con capacidades distintas, o en cualquier caso, discriminatorio.
* Publicar o exhibir material sexualmente explícito o violento.
* Publicar o amenazar con publicar la información personal de otras personas sin su consentimiento, incluyendo direcciones físicas o electronicas ("doxing").
* Insultos personales, particularmente aquellos relacionados con el género, orientación sexual, raza, religión o discapacidad.
* Fotografías o grabaciones inapropiadas.
* Contacto físico inapropiado. Se debe tener el consentimiento del otr@ antes de tocarl@.
* Acercamiento sexual no deseado. Esto incluye comentarios sexualizados o bromas; Manoseos, tocaciones y proposiciones sexuales no correspondidas.
* Intimidación deliberada, acoso físico, seguimiento u hostigamiento (en línea o en persona).
* Acoso público o privado
* Promover o alentar, cualquiera de los comportamientos anteriores.
* Interrupción sostenida de los eventos de la comunidad, incluyendo charlas y presentaciones.
* Compartir cualquier material sin derechos de autor.
* Publicar enlaces externos como blogs, sitios web o foros sin una razón lógica que haga sentido colectivo, sin derechos de autor, o promover el uso de recursos o material ilegal.
* Comentarios insultantes o despectivos (*trolling*) y ataques personales, religiosos o políticos.
* Otros tipos de conducta que pudieran considerarse inapropiadas en un entorno profesional.

## 5. Política de armas

No se permitirán armas en los eventos de Programadores Chile, espacios comunitarios, u otros espacios cubiertos por el alcance de este Código de Conducta. Dentro de las armas se incluyen, pero no se limita, las armas de fuego, explosivos (incluyendo fuegos artificiales), y armas blancas, tales como las utilizadas para la caza o la exhibición, así como cualquier otro elemento utilizado con el propósito de causar lesión o daño a otr@s. A cualquiera que se encuentre en posesión de uno de estos artículos se le pedirá que se retire inmediatamente y sólo se le permitirá regresar sin el arma. Se espera que l@s asistentes cumplan con todas las leyes estatales y locales en esta materia.

## 6. Consecuencias de un comportamiento inaceptable

Los administradores de Programadores Chile son responsables de clarificar los estándares de comportamiento aceptable y se espera que tomen medidas correctivas y apropiadas en respuesta a situaciones de conducta inaceptable. 

Los administradores del Programadores Chile tienen el derecho y la responsabilidad de eliminar, editar o rechazar comentarios, *commits*, código, ediciones de documentación, *issues*, y otras contribuciones que no estén alineadas con este Código de Conducta, o de prohibir temporal o permanentemente a cualquier colaborador cuyo comportamiento sea inapropiado, amenazante, ofensivo o perjudicial.

Los comportamientos inaceptables de cualquier miembro de la comunidad, incluyendo los patrocinadores y los miembros que tengan autoridad para tomar decisiones, no serán tolerados.

Cualquier persona a quién se le solicite detener su comportamiento inaceptable, se espera que acate inmediatamente.

Si un miembro de la comunidad es parte de un comportamiento inaceptable, se tomarán las medidas apropiadas, llegando incluso a la prohibición temporal o expulsión permanente de la comunidad sin previo aviso (y sin reembolso en el caso de un evento pagado).

En el caso de que una publicación infrinja la pauta del Código de Conducta, estas serán eliminadas sin previo aviso.

## 7. Guía de Denuncias

Si eres víctima o testigo de comportamientos inaceptables o bien posees alguna otra inquietud, por favor contacta a un organizador de la comunidad lo antes posible.

Además, las y los organizadores de la comunidad están disponibles para ayudar a los miembros de la comunidad a contactarse con la policía local o bien para ayudar a aquell@s que son víctimas de comportamientos inaceptables a sentirse seguros. En el caso de los eventos presenciales, las y los organizadores proveerán escoltas en cuanto la persona afectada así lo desee.

## 8. Tratamiento de Quejas

Si sientes que has sido falsamente o injustamente acusad@ de violar este Código de Conducta, debes notificar a los moderadores con una breve descripción de su queja. Su queja será revisada en concordancia con nuestras políticas de moderación.

El equipo de Programadores Chile está obligado a mantener confidencialidad de la persona que reportó el incidente. Detalles específicos acerca de las políticas de aplicación pueden ser publicadas por separado.

Administradores que no sigan o que no hagan cumplir este Código de Conducta pueden ser eliminados de forma temporal o permanente del equipo. 

## 9. Alcances

Esperamos que todos las y los participantes de la comunidad (contribuyentes, pagados o no, patrocinadores y otros invitados) cumplan este Código de Conducta en todas las instancias de la comunidad --tanto en línea como presenciales-- así como en todas las comunicaciones personales que se vinculen a la comunidad.

Este código de conducta y sus procedimientos relacionados también aplican a los comportamientos inaceptables que ocurren fuera del contexto de las actividades de la comunidad cuando tales comportamientos tengan el potencial de atentar contra la seguridad y el bienestar de los miembros de la comunidad.

Este código de conducta aplica tanto a espacios de la comunidad como a espacios públicos donde un individuo esté en representación de la comunidad. Ejemplos de esto incluye el uso de la cuenta oficial de correo electrónico, publicaciones a través de las redes sociales oficiales, o presentaciones con personas designadas en eventos *online* u *offline*. La representación de Programadores Chile puede ser clarificada explícitamente por los administradores de la comunidad.

## 10. Licencia y atribución

Partes del texto fueron basadas en [Django Code of Conduct](https://www.djangoproject.com/conduct/), en [Geek Feminism Anti-Harassment Policy](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy), en Este Código de Conducta es una adaptación del [Contributor Covenant][homepage], versión 1.4, disponible en [http://contributor-covenant.org/version/1/4/es/][version], y en [Stumptown Syndicate](http://stumptownsyndicate.org) bajo [Creative Commons Attribution-ShareAlike license](http://creativecommons.org/licenses/by-sa/3.0/).
[Monografias.com](http://www.monografias.com/trabajos98/etica-del-profesional-informatico-reflexion-etica-del-deber-moral/etica-del-profesional-informatico-reflexion-etica-del-deber-moral.shtml)
[Computer.org](http://chapters.computer.org/dominicana/contribuciones/Codigo_Etica_Ing_Software.pdf)