# Pautas Oficiales de la Comunidad
---

Las pautas se han generado con el fin de mantener un orden dentro de la comunidad y para que cualquier persona que desee encontrar un tema en específico, sea capaz de encontrarlo de la manera más rápida posible.

- **Pauta de Oferta Laboral Contrato Empresa:**

```
Nombre empresa:

Puesto Ofrecido:

Perfil Requerido:

Rango de renta ofrecida (aproximación):

Horario:

Ubicación:

Remoto/Presencial:

Beneficios que se entregarán:

Tipo de contrato:
```
 
- **Pauta de Oferta Laboral Freelance:**

```
Nombre empresa:

Perfil requerido:

Fechas estimadas:

Remoto/Presencial:

Renta ofrecida por proyecto/por hora (aproximación):

Descripción del proyecto
```

- **Pauta general de consultas:**

```
[Nombre Framework/Lenguaje/Tópico]

Consulta
```

### Para ambas ofertas laborales, el campo *renta ofrecida* es obligatorio.

**Referentes**

[Pautas Oficiales ProIn](https://github.com/proinchile/CodigosDeConducta/blob/master/pautas_oficiales.md)